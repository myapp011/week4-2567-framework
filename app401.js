const days = ["Monday","Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
const weeks = ["one","Two", "Three", "Four", "Five", "Six", "Seven"]

//Destructuring assignment
const [day1,,day3,,,day7] = days
console.log(day1,day3,day7)

//Destructuring assignment with rest operator
const [week1,,week3,,,args] = weeks
console.log(week1)
console.log(week3)
console.log(args);