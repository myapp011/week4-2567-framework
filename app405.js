//Merg/Concat
const a1 = [1,2,3]
const a2 = [4,5,6]
const a3 = 7
const user1 = { name: "Mark Zuckerberg" };
const user2 = { photo: "1.jpg" };

const arrC = [...a1, ...a2,a3]; //[1,2,3,4,5,6,7]
console.log(arrC)

const users = { ...user1, ...user2 };
const {usera, userb } = users;
console.log(users)